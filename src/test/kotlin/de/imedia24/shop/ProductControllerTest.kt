package de.imedia24.shop

import de.imedia24.shop.controller.ProductController
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito
import org.mockito.InjectMocks
import org.mockito.MockitoAnnotations
import org.skyscreamer.jsonassert.JSONAssert
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.RequestBuilder
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import java.lang.Exception
import java.math.BigDecimal
import java.util.ArrayList

@RunWith(SpringRunner::class)
@WebMvcTest(ProductController::class)
class ProductControllerTest {

    private val sku = "123"
    var name = "idee projet"
    var description = "description"
    var stock = "stock"
    var price = BigDecimal("20.00")

    @Autowired
    var wac: WebApplicationContext? = null
    private var mockMvc: MockMvc? = null

    @MockBean
    private val productService: ProductService? = null

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mockMvc = MockMvcBuilders.webAppContextSetup(wac!!).build()
    }

    @Test
    @Throws(Exception::class)
    fun shouldGetProductWithSuccess() {
        // Given
        val productResponseList: MutableList<ProductResponse> = ArrayList<ProductResponse>()
        val productResponse: ProductResponse = buildProductResponse()
        productResponseList.add(productResponse)
        val productResponseList1: MutableList<ProductResponse> = ArrayList<ProductResponse>()
        val productResponse1: ProductResponse = buildProductResponse()
        productResponseList1.add(productResponse1)
        if (productService != null) {
            BDDMockito.given(productService.findProductBySku(sku)).willReturn(productResponse)
        }

        // When
        val requestBuilder: RequestBuilder = MockMvcRequestBuilders.get("/products/{sku}")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON, MediaType.valueOf("application/x-spring-data-verbose+json"))
        val result = mockMvc!!.perform(requestBuilder).andReturn()

        // Then
        Assert.assertEquals(HttpStatus.OK.value().toLong(), result.response.status.toLong())
    }

    private fun buildProductResponse(): ProductResponse {
        val productResponse = ProductResponse(
            sku = sku,
            name = name,
            description = description,
            price = price,
            stock = stock
        )
        productResponse.sku = sku
        productResponse.name = name
        productResponse.description = description
        productResponse.price = price
        productResponse.stock = stock
        return productResponse
    }
}