package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductEntity
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import lombok.AllArgsConstructor
import org.springframework.stereotype.Service
import java.util.*

@Service
@AllArgsConstructor
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse? {
        val productsBySku: ProductEntity = productRepository.findBySku(sku);
        return productsBySku.toProductResponse()
    }

    fun findProductsBySkuList(skuList: String): ProductResponse? {
        val productsBySkuList: ProductEntity = productRepository.findProductsBySku(skuList);
        return productsBySkuList.toProductResponse()
    }

    fun createProduct(product: ProductResponse): ProductResponse? {
        val newProduct: ProductEntity = product.toProductEntity()
        val result: ProductEntity = productRepository.save(newProduct)
        return result.toProductResponse()
    }

    fun updateProduct(product: ProductResponse, sku: String): ProductResponse? {
        val productToUpdate: ProductEntity = product.toProductEntity()
//        productToUpdate.setSku(sku)
        val result: ProductEntity = productRepository.save(productToUpdate)
        return result.toProductResponse()
    }
}
