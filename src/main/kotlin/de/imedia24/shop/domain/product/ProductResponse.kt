package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.ProductEntity
import lombok.AllArgsConstructor
import lombok.Builder
import lombok.Data
import lombok.NoArgsConstructor
import java.math.BigDecimal
import java.time.ZonedDateTime

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
data class ProductResponse(
    var sku: String,
    var name: String,
    var description: String,
    var stock: String,
    var price: BigDecimal
) {
    companion object {
        fun ProductEntity.toProductResponse() = ProductResponse(
            sku = sku,
            name = name,
            description = description ?: "",
            stock = stock,
            price = price
        )
        fun ProductResponse.toProductEntity() = ProductEntity(
            sku = sku,
            name = name,
            description = description ?: "",
            stock = stock,
            price = price,
            createdAt = ZonedDateTime.now(),
            updatedAt = ZonedDateTime.now()
        )
    }
}
