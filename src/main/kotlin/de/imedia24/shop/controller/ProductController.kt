package de.imedia24.shop.controller

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/product/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku);
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @GetMapping("/products/sku={skuList}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySkuList(
        @PathVariable("skuList") skuList: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $skuList")

        val product = productService.findProductsBySkuList(skuList)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @PostMapping("/newProduct", produces = ["application/json;charset=utf-8"])
    fun addProduct(
        @RequestBody product: ProductResponse
    ): ResponseEntity<ProductResponse> {
        logger.info("Add new product")

        productService.createProduct(product)
        return ResponseEntity.ok(product)
    }

    @PutMapping("/editProduct/{sku}", produces = ["application/json;charset=utf-8"])
    fun updateProduct(
        @RequestBody product: ProductResponse,
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Update product")

        productService.updateProduct(product, sku)
        return ResponseEntity.ok(product)
    }
}
