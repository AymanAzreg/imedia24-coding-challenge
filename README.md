# iMedia24 Coding challenge

### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Official Kotlin documentation](https://kotlinlang.org/docs/home.html)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.3/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.3/gradle-plugin/reference/html/#build-image)
* [Flyway database migration tool](https://flywaydb.org/documentation/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

### Running the Docker Container

* To run the application container, simply execute the run.sh file in your command prompt : ./run.sh

* To get the list of all active containers, type this command : docker ps

* To stop the container, type the following command : docker stop [Name of the container]
* Exemple : docker stop friendly_fox

* To remove the container, type the following command : docker rm [Name of the container]
* Exemple : docker stop friendly_fox.

Note: The container ID can be used instead of the container name. No need to type the full ID, the first 3 characters are enough to make the commands work.
